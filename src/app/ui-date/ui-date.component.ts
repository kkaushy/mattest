import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
// import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-ui-date',
  templateUrl: './ui-date.component.html',
  styleUrls: ['./ui-date.component.css']
})
export class UiDateComponent implements OnInit {

  ngOnInit(): void {
    this.onChangeDate();
  }

  public days:number[]=[];
  public months:Array<{"name":string, "value":number}>;
  public years:number[]=[];
  public year : number=2021;
  public month = 1;
  public day=1;
  public message = "";
  

  constructor(){
    
    for(let i=1;i<=31;i++){
      this.days.push(i);
    }
    this.months = [{
        "name": "January",
        "value":1
      },{
        "name": "February",
        "value":2
      },{
        "name": "March",
        "value":3
      },{
        "name": "April",
        "value":4
      },{
        "name": "May",
        "value":5
      },{
        "name": "June",
        "value":6
      },{
        "name": "July",
        "value":7
      },{
        "name": "August",
        "value":8
      },{
        "name": "September",
        "value":9
      },{
        "name": "October",
        "value":10
      },{
        "name": "November",
        "value":11
      },{
        "name": "December",
        "value":12
      },];
    
    for(let i=2021;i>=1940;i--){
      this.years.push(i);
    }
  }
  onChangeDate (){ 
    let objMoment = moment(this.year.toString() + this.month.toString().padStart(2,"0") + this.day.toString().padStart(2,"0"), "YYYYMD")
    if (objMoment.isValid()){
      this.message = `is a ${objMoment.format("dddd")}`;  
    }else{
      this.message = "is not a valid date";  
    }
  }
}
